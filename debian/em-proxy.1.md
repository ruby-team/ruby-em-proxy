% EM-PROXY(1) em-proxy Man Page
% Philippe Thierry
% June 2017
# NAME
em-proxy - EventMachine Proxy DSL for writing high-performance transparent / intercepting proxies in Ruby

# SYNOPSIS

Usage:
**backdoor-factory [options]**

# DESCRIPTION

**em-factory** is an EventMachine Proxy DSL for writing high-performance transparent / intercepting proxies in Ruby.
It permits one to implement filtering various protocol such as SMTP, etc. and support duplication traffic duplication.
It can be use as a standalone tool or as a ruby library.

# OPTIONS

**-l, --listen [PORT]**
Port to listen on

**-d, --duplex [host:port, ...]**
List of backends to duplex data to

**-r, --relay [hostname:port]**
Relay endpoint: hostname:port

**-s, --socket [filename]**
Relay endpoint: unix filename

**-v, --verbose**
Run in debug mode

#EXAMPLE

The following will start em-proxy on port 8080, relay and respond with data from port 8081, and also (optional) duplicate all traffic to ports 8082 and 8083 (and discard their responses).

`em-proxy -l 8080 -r localhost:8081 -d localhost:8082,localhost:8083 -v`

# HISTORY

June 2017, Man page originally written by Philippe Thierry (phil at reseau-libre dot com)
